package com.hernan.app.excerciserestssr.dto;

public class ActorDTO {
	
	String name;
	String apellido;
	
	public ActorDTO() {}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;

}
}
