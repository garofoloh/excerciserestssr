package com.hernan.app.excerciserestssr.dto;

import java.util.Date;
import java.util.List;


public class PeliculaDTO {
	
	private String name;
	private String pais;
	private Date fechaEstreno;
	private String director;
	private List<ActorDTO> reparto;
	
	
	public PeliculaDTO() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getFechaEstreno() {
		return fechaEstreno;
	}

	public void setFechaEstreno(Date fechaEstreno) {
		this.fechaEstreno = fechaEstreno;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public List<ActorDTO> getReparto() {
		return reparto;
	}

	public void setReparto(List<ActorDTO> reparto) {
		this.reparto = reparto;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}
	
	

}
