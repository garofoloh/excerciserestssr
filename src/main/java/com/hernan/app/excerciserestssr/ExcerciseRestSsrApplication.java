package com.hernan.app.excerciserestssr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcerciseRestSsrApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExcerciseRestSsrApplication.class, args);
	}

}
