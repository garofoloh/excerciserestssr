package com.hernan.app.excerciserestssr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hernan.app.excerciserestssr.models.Actor;

public interface ActoresRepository extends JpaRepository<Actor, Long> {
	
	Actor findByName(String name);

}
