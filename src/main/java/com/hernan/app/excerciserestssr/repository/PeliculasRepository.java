package com.hernan.app.excerciserestssr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.hernan.app.excerciserestssr.models.Pelicula;

public interface PeliculasRepository extends JpaRepository<Pelicula, Long>, PagingAndSortingRepository<Pelicula, Long> {
	
	
	Pelicula findByName(String name);

}
