package com.hernan.app.excerciserestssr.serviceImpl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hernan.app.excerciserestssr.converter.PeliculaConverter;
import com.hernan.app.excerciserestssr.dto.PeliculaDTO;
import com.hernan.app.excerciserestssr.models.Pelicula;
import com.hernan.app.excerciserestssr.repository.PeliculasRepository;
import com.hernan.app.excerciserestssr.service.IPeliculaService;

@Service
public class PeliculaServiceImpl implements IPeliculaService {
	
	@Autowired
	PeliculasRepository peliculasRepository;


	@Override
	public List<PeliculaDTO> getAll(int page, int size) throws Exception {
		
		    
		List<Pelicula>list = (List<Pelicula>) peliculasRepository.findAll();
		List<PeliculaDTO> lista= list.stream().map(p -> PeliculaConverter.toDTO(p)).collect(Collectors.toList());
		Collections.sort(lista, new Comparator<PeliculaDTO>() {
			   public int compare(PeliculaDTO obj1, PeliculaDTO obj2) {
			      return obj1.getName().compareTo(obj2.getName());
			   }
			});
		int pageIndex = page*size;
		if((lista.size()/size) < page) {
			pageIndex = (Math.abs(lista.size()/size))*size;
		}
		
		
		int sizeFinal = pageIndex+size;
		if(sizeFinal > lista.size()) {
			sizeFinal =lista.size();
		}
		List<PeliculaDTO> subList = lista.subList(pageIndex, sizeFinal);
		return subList;
	}
	
	
	
	public PeliculaDTO getePeliculaByName(String name) throws Exception {
		
		return PeliculaConverter.toDTO(peliculasRepository.findByName(name));
	}


	@Override
	public PeliculaDTO addPelicula(PeliculaDTO peliculaDto) throws Exception {
		return PeliculaConverter.toDTO(peliculasRepository.save(PeliculaConverter.toModel(peliculaDto)));
		
	}

	@Override
	public PeliculaDTO updatePelicula(PeliculaDTO peliculaDto) throws Exception {
		PeliculaDTO respuesta = null;
		Pelicula pelicula = peliculasRepository.findByName(peliculaDto.getName());
		if(pelicula.getId() != null) {
			respuesta = PeliculaConverter.toDTO(peliculasRepository.save(PeliculaConverter.toModel(peliculaDto)));
		}
		return respuesta;
	}

	@Override
	public void deletePeliculaByName(String name) throws Exception {
		Pelicula pelicula = (Pelicula)peliculasRepository.findByName(name);
		peliculasRepository.delete(pelicula);
		
	}

}
