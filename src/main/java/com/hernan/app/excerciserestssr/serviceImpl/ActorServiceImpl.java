package com.hernan.app.excerciserestssr.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hernan.app.excerciserestssr.converter.ActorConverter;
import com.hernan.app.excerciserestssr.dto.ActorDTO;
import com.hernan.app.excerciserestssr.models.Actor;
import com.hernan.app.excerciserestssr.repository.ActoresRepository;
import com.hernan.app.excerciserestssr.service.IActorService;

@Service
public class ActorServiceImpl implements IActorService {
	@Autowired
	ActoresRepository actoresRepository;
	

	public List<ActorDTO> getAll() throws Exception {

		List<Actor>list = actoresRepository.findAll();
		return list.stream().map(p -> ActorConverter.toDTO(p)).collect(Collectors.toList());
	}


	
	public ActorDTO getActorByName(String name) throws Exception {
		return ActorConverter.toDTO(actoresRepository.findByName(name));
	}

}
