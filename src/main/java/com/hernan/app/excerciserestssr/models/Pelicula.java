package com.hernan.app.excerciserestssr.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name= "peliculas")
public class Pelicula implements Serializable, Comparable {
	
		
		
		private static final long serialVersionUID = -2996650481849762286L;
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long id;
		private String name;
		private String pais;
		
		@Column(name = "fecha_estrenos")
		@Temporal(TemporalType.DATE)
		private Date fechaEstreno;
		
		private String director;
		
		@JoinColumn(name="actores", referencedColumnName = "id")
		@OneToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
		private List<Actor> reparto;
		
		public Pelicula() {		}
		
		public Long getId() {return id;}
		public void setId(Long id) {this.id = id;}
		public String getName() {return name;}
		public void setName(String name) {this.name = name;}
		public Date getFechaEstreno() {return fechaEstreno;}
		public void setFechaEstreno(Date fechaEstreno) {this.fechaEstreno = fechaEstreno;}
		public String getDirector() {return director;}
		public void setDirector(String director) {this.director = director;}
		public List<Actor> getReparto() {return reparto;}
		public void setReparto(List<Actor> reparto) {this.reparto = reparto;}
		
		
		@Override
		public int compareTo(Object o) {
			// TODO Auto-generated method stub
			return 0;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Pelicula other = (Pelicula) obj;
			if (director == null) {
				if (other.director != null)
					return false;
			} else if (!director.equals(other.director))
				return false;
			if (fechaEstreno == null) {
				if (other.fechaEstreno != null)
					return false;
			} else if (!fechaEstreno.equals(other.fechaEstreno))
				return false;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			if (reparto == null) {
				if (other.reparto != null)
					return false;
			} else if (!reparto.equals(other.reparto))
				return false;
			return true;
		}

		public String getPais() {
			return pais;
		}

		public void setPais(String pais) {
			this.pais = pais;
		}
		
		

	}


