package com.hernan.app.excerciserestssr.converter;

import com.hernan.app.excerciserestssr.dto.ActorDTO;
import com.hernan.app.excerciserestssr.models.Actor;

public class ActorConverter {
	
	public static ActorDTO toDTO(Actor actor) {
		ActorDTO actorDto = new ActorDTO();
		actorDto.setName(actor.getName());
		actorDto.setApellido(actor.getApellido());
		
		return actorDto;
		
	}
	
	public static Actor toModel(ActorDTO actorDto) {
		Actor actor = new Actor();
		actor.setName(actorDto.getName());
		actor.setApellido(actorDto.getApellido());
		
		return actor;
		
	}

}
