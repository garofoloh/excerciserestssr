package com.hernan.app.excerciserestssr.converter;

import java.util.List;
import java.util.stream.Collectors;

import com.hernan.app.excerciserestssr.dto.ActorDTO;
import com.hernan.app.excerciserestssr.dto.PeliculaDTO;
import com.hernan.app.excerciserestssr.models.Actor;
import com.hernan.app.excerciserestssr.models.Pelicula;

public class PeliculaConverter {
	
	public static PeliculaDTO toDTO(Pelicula pelicula) {
		PeliculaDTO peliculaDto = new PeliculaDTO();
		peliculaDto.setName(pelicula.getName());
		peliculaDto.setDirector(pelicula.getDirector());
		peliculaDto.setFechaEstreno(pelicula.getFechaEstreno());
		peliculaDto.setReparto(listDto(pelicula.getReparto()));
		peliculaDto.setPais(pelicula.getPais());
		
		return peliculaDto;
		
	}
	
	public static Pelicula toModel(PeliculaDTO peliculaDTO) {
		Pelicula pelicula = new Pelicula();
		pelicula.setName(peliculaDTO.getName());
		pelicula.setDirector(peliculaDTO.getDirector());
		pelicula.setFechaEstreno(peliculaDTO.getFechaEstreno());
		pelicula.setReparto(listModel(peliculaDTO.getReparto()));
		pelicula.setPais(peliculaDTO.getPais());
		return pelicula;
		
	}
	
	private static List<ActorDTO> listDto(List<Actor> lista){
		return (List<ActorDTO>) lista.stream().map(l -> ActorConverter.toDTO(l)).collect(Collectors.toList());
	}
	
	private static List<Actor> listModel(List<ActorDTO> lista){
		return (List<Actor>) lista.stream().map(l -> ActorConverter.toModel(l)).collect(Collectors.toList());
	}


}
