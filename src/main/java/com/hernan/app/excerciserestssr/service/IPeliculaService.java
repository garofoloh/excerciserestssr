package com.hernan.app.excerciserestssr.service;

import java.util.List;

import com.hernan.app.excerciserestssr.dto.PeliculaDTO;


public interface IPeliculaService {
	
	public List <PeliculaDTO> getAll(int page, int size) throws Exception;
	public PeliculaDTO getePeliculaByName(String name) throws Exception;
	public PeliculaDTO addPelicula(PeliculaDTO peliculaDto) throws Exception;
	public PeliculaDTO updatePelicula(PeliculaDTO peliculaDto) throws Exception;
	public void deletePeliculaByName(String name) throws Exception;
	

}
