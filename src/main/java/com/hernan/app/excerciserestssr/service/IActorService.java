package com.hernan.app.excerciserestssr.service;

import java.util.List;

import com.hernan.app.excerciserestssr.dto.ActorDTO;


public interface IActorService {
	public List<ActorDTO> getAll() throws Exception;
	public ActorDTO getActorByName(String name) throws Exception;
}
