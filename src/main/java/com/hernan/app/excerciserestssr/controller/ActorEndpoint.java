package com.hernan.app.excerciserestssr.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.hernan.app.excerciserestssr.dto.ActorDTO;


public interface ActorEndpoint {
	public String GET_ALL_ACTORES ="/get-all-actores";
	public String GET_ACTOR_BY_NAME ="/get-actor-by-name";
	 
	 public List <ActorDTO> listar() throws Exception;
	 public ActorDTO getActorByName(String name) throws Exception;

}
