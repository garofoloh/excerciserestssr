package com.hernan.app.excerciserestssr.controller;

import java.util.List;

import com.hernan.app.excerciserestssr.dto.PeliculaDTO;

public interface PeliculaEndpoint {
	 public String GET_PELICULAS ="/get-all-peliculas";
	 public String GET_PELICULAS_BY_NOMBRE ="/get-peliculas-by-nombre";
	 public String ADD_PELICULA ="/add-pelicula";
	 public String UPDATE_PELICULA ="/update-peliculas";
	 public String DELETE_PELICULAS_BY_NOMBRE ="/delete-pelicula-by-nombre";
	
	 
	 public List <PeliculaDTO> listar(int page, int size) throws Exception;
	 public PeliculaDTO getPeliculaByName(String name) throws Exception;
	 public PeliculaDTO addPelicula(PeliculaDTO peliculaDto) throws Exception;
	 public PeliculaDTO updatePelicula(PeliculaDTO peliculaDto) throws Exception;
	 public void deletePeliculaByName(String name) throws Exception;

}
