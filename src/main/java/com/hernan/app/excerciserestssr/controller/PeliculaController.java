package com.hernan.app.excerciserestssr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hernan.app.excerciserestssr.dto.PeliculaDTO;
import com.hernan.app.excerciserestssr.service.IPeliculaService;



@RestController
@RequestMapping(value = "/v1")
public class PeliculaController implements PeliculaEndpoint {
	
	@Autowired
	IPeliculaService peliculaService;
	
	@GetMapping(value = PeliculaEndpoint.GET_PELICULAS, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public List <PeliculaDTO> listar(@RequestParam int page, @RequestParam int size) throws Exception{
		
		return peliculaService.getAll(page, size);
	}

	@GetMapping(value = PeliculaEndpoint.GET_PELICULAS_BY_NOMBRE, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public PeliculaDTO getPeliculaByName(@RequestParam String name) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@PostMapping(value = PeliculaEndpoint.ADD_PELICULA, produces = MimeTypeUtils.APPLICATION_JSON_VALUE,
			consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public PeliculaDTO addPelicula(@RequestBody PeliculaDTO peliculaDto) throws Exception {
		return peliculaService.addPelicula(peliculaDto);
		
	}

	@GetMapping(value = PeliculaEndpoint.UPDATE_PELICULA, produces = MimeTypeUtils.APPLICATION_JSON_VALUE,
			consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public PeliculaDTO updatePelicula(@RequestBody PeliculaDTO peliculaDto) throws Exception {
		return peliculaService.updatePelicula(peliculaDto);
	}

	@PostMapping(value = PeliculaEndpoint.DELETE_PELICULAS_BY_NOMBRE)
	public void deletePeliculaByName(@RequestParam String name) throws Exception {
		peliculaService.deletePeliculaByName(name);
		
	}

}
