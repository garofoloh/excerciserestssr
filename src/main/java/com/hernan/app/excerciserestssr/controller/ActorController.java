package com.hernan.app.excerciserestssr.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hernan.app.excerciserestssr.dto.ActorDTO;
import com.hernan.app.excerciserestssr.service.IActorService;

@RestController
@RequestMapping(value = "/v1")
public class ActorController implements ActorEndpoint {
	
	@Autowired
	IActorService actorService;

	@Override
	@GetMapping(value = ActorEndpoint.GET_ALL_ACTORES, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public List<ActorDTO> listar() throws Exception {

		return actorService.getAll();
	}
	@GetMapping(value = ActorEndpoint.GET_ACTOR_BY_NAME, produces = MimeTypeUtils.APPLICATION_JSON_VALUE)
	public ActorDTO getActorByName(@RequestParam String name) throws Exception {

		return actorService.getActorByName(name);
	}
	
	

}
